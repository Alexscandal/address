Форма адреса
============
Форма адреса с подгрузкой данных из БД

Installation
------------

The preferred way to install this extension is through [composer](https://getcomposer.org/download/).

Either run

```
php composer.phar require --prefer-dist alexscandal/yii2-address "*"
```

or add

```
"alexscandal/yii2-address": "*"
```

to the require section of your `composer.json` file.


Usage
-----

Once the extension is installed, simply use it in your code by  :

```php
<?= \alexscandal\address\AutoloadExample::widget(); ?>```
